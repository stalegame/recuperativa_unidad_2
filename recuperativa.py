import gi
import re
import json
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

# funcion para cargar el json con los datos del contacto
def cargar_datos():
    try:

        with open('Agenda.json') as file:
            data = json.load(file)

    except IOError:
        data = {}

    return data

# Funcion para guardar los datos del contacto
def guardar_datos(data):

    with open('Agenda.json', 'w') as file:
        json.dump(data, file, indent=4)


class MainWindow():

    # Se crea el constructor de la ventana principal
    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('telefono.ui')

        # Ventana
        self.window = self.builder.get_object('window')
        self.window.set_title('Telefono')
        self.window.connect('destroy', Gtk.main_quit)
        self.window.set_default_size(800, 300)

        self.data = cargar_datos()

        # botones
        self.btn_cancel = self.builder.get_object('btn_cancel')
        self.btn_cancel.connect('clicked', self.btn_cancel_clicked)

        self.btn_yes = self.builder.get_object('btn_yes')
        self.btn_yes.connect('clicked', self.btn_yes_clicked)

        self.btn_save = self.builder.get_object('btn_save')
        self.btn_save.connect('clicked', self.btn__save_clicked)

        # label
        self.label = self.builder.get_object('label')

        # entry
        self.entry = self.builder.get_object('entry')
        self.entry.set_max_length(8)
        self.entry.connect('changed', self.validar)

        botones = []
        # se usa el for para reconocer los 10 botones numericos y a la vez
        # llamar a la funcion para reconocer cada valor de los botones en el
        # entry
        for i in range(10):
            botones.append(self.builder.get_object(f'{i}'))
            botones[i].connect('clicked', self.marcar, str(i))

        self.window.show_all()

    # Funcion para llamar al numero, si este esta registrado, muestra los
    # datos del contacto
    def btn_yes_clicked(self, btn=None):

        numero = self.entry.get_text()

        # Saca de la data, si esta guardada en el json y la envia a llamar
        if numero in self.data:
            nombre = self.data[numero]['nombre']
            alias = self.data[numero]['alias']

        else:
            nombre = None
            alias = None

        Dialog_Llamar(numero, nombre, alias)

    # la funcion es para ingresar y poner el valor del boton en el entry
    def marcar(self, btn=None, value=None):
        self.entry.set_text(self.entry.get_text()+value)


    # la funcion es para evitar que se puedan poner valores no numericos
    def validar(self, btn=None):
        # https://stackoverflow.com/questions/17336943/removing-non-numeric-characters-from-a-string
        # Se utilizo esta referencia
        self.entry.set_text(''.join(re.findall('[0-9]',self.entry.get_text())))

        numero = self.entry.get_text()

        if numero in self.data:

            self.label.set_text(f"Nombre: {self.data[numero]['nombre']}\n"
                                f"Alias: {self.data[numero]['alias']}\n"
                                f"Correo: {self.data[numero]['correo']}\n"
                                f"Trabajo: {self.data[numero]['work']}")
        else:
            self.label.set_text('')


    # simplemente limpia el entry
    def btn_cancel_clicked(self, btn=None):
        self.entry.set_text('')


    # Se abre el dialogo y guarda o cancela la operacion, de dicha ventana
    def btn__save_clicked(self, btn=None):

        # Se mantiene abierta la ventana de dialogo esperando una respuesta
        dialog = DialogWindow()
        response = dialog.window.run()

        """ Si se apreta el boton aceptar pueden ocurrir varios sucesos, uno de
        ellos es que los entrys se encuentren vacios, por ende saca una ventana
        de dialogo de error, sino pasa esto, la informacion de los entry se
        guarda en el archivo json"""
        if response == Gtk.ResponseType.OK:
            datos = dialog.get_data()
            valores = [x.strip() for x in datos.values()]

            if '' in valores:
                Error_Dialog()

            else:

                # Se guarda la info respecto al numero que se ingrese
                numero = int(self.entry.get_text())
                self.data[numero] = {}
                self.data[numero]['nombre'] = datos['nombre']
                self.data[numero]['alias'] = datos['alias']
                self.data[numero]['correo'] = datos['correo']
                self.data[numero]['work'] = datos['work']

                guardar_datos(self.data)
                self.data = cargar_datos()
                self.validar()

class DialogWindow():

    # Contructor de la ventana de dialogo
    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('telefono.ui')

        # Ventana
        self.window = self.builder.get_object('dialog')

        # Entrys
        self.name = self.builder.get_object('name')
        self.alias = self.builder.get_object('alias')
        self.correo = self.builder.get_object('correo')
        self.work = self.builder.get_object('work')

        # Btn
        self.cancel = self.builder.get_object('dialog_cancel')
        self.cancel.connect('clicked', self.cancelar)

        self.window.show_all()


    def cancelar(self, btn=None):
        self.window.destroy()


    # guarda y pasa la informacion a otra ventana
    def get_data(self):

        datos = {}

        datos['nombre'] = self.name.get_text()
        datos['alias'] = self.alias.get_text()
        datos['correo'] = self.correo.get_text()
        datos['work'] = self.work.get_text()

        self.window.destroy()

        return datos


# Ventana de dialogo para señalar que no se llenaron los entry
class Error_Dialog():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('telefono.ui')

        # Ventana
        self.window = self.builder.get_object('error_dialog')

        # btn
        self.aceptar = self.builder.get_object('aceptar')
        self.aceptar.connect('clicked', self.cliked_aceptar)

        self.window.show_all()

    def cliked_aceptar(self, btn=None):
        self.window.destroy()


class Dialog_Llamar():

    def __init__(self, numero, nombre=None, alias=None):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('telefono.ui')

        # Ventana
        self.window = self.builder.get_object('llamar')

        # btn
        self.colgar = self.builder.get_object('btn_colgar')
        self.colgar.connect('clicked', self.cortar)

        # Dar informacion si existe, o no
        if nombre is not None:
            self.window.set_property('text', f"Llamando a {alias}")
            self.window.format_secondary_text(f"{nombre} - {numero}")

        else:
            self.window.set_property('text', f"Llamando a {numero}")

        self.window.show_all()

    def cortar(self, btn=None):
        self.window.destroy()


if __name__ == "__main__":
    MainWindow()
    Gtk.main()
